% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot.R
\name{groupPrevBoxPlot}
\alias{groupPrevBoxPlot}
\title{Plot a box plot with a desired y-axis to compare with prevalence and group size}
\usage{
groupPrevBoxPlot(
  run.name,
  perc.all,
  fig.file.name,
  y.axis = "sensitivity",
  y.axis.name = "Sensitivity \%",
  x.axis.name = "Group size",
  ylims = c(70, 100),
  brewer.pallete = "BrBG",
  show.x.axis = TRUE
)
}
\description{
Plot a box plot with a desired y-axis to compare with prevalence and group size
}
