
First, it is necessary to install the *webshot2* dependency that is not on CRAN: 

```{r}
library(remotes)
install_github("rstudio/webshot2"))
```

Then, to install *poolingr* R package directly from bitbucket use:

```{r}
install_bitbucket("viniciushdasilva/poolingr", build_vignettes = TRUE)
```

The folder vignettes contain usage examples with the data used in the manuscript 'Simulation of group testing scenarios can boost COVID-19 screening power' (doi to be included). After the package installation you can access the vignette with:

```{r}
vignette("poolingr")
```


